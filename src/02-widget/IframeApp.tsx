import React, { useEffect, useRef } from 'react'
import { COMMUNITATION_KEY } from './Widget'
import { CONTENT_REFRESH_INTERVAL, TARGET_ORIGIN } from './const'
import { randomSentence } from './utils'

export const IframeApp = () => {
  const iframeContainer = useRef<HTMLDivElement | null>(null)
  const [content, setContent] = React.useState(randomSentence())

  const updateHeight = () => {
    const measurements = iframeContainer.current?.getBoundingClientRect()
    if (parent.postMessage && measurements) {
      const newHeight = Number(measurements?.height)
      parent.postMessage(
        JSON.stringify({
          message: COMMUNITATION_KEY,
          height: newHeight > 0 ? newHeight + 20 : -1, // +20 to prevent scrollbar
        }),
        TARGET_ORIGIN
      )
    }
  }

  useEffect(() => {
    const contentInterval = setInterval(() => {
      setContent(randomSentence())
    }, CONTENT_REFRESH_INTERVAL)
    return () => clearInterval(contentInterval)
  }, [])

  useEffect(() => {
    updateHeight()
  }, [updateHeight, iframeContainer.current])

  useEffect(() => {
    window.addEventListener('resize', updateHeight)
    return () => window.removeEventListener('resize', updateHeight)
  }, [window, updateHeight])

  return (
    <div
      ref={iframeContainer}
      className="iframeWrapper"
    >
      {content}
    </div>
  )
}
