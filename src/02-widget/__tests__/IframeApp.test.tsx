import React from 'react'
import { render, act } from '@testing-library/react'
import '@testing-library/jest-dom'
import { IframeApp } from '../IframeApp'

jest.mock('react', () => ({
  ...jest.requireActual('react'),
  useState: () => ['Test sentence', jest.fn()],
}))

describe('IframeApp', () => {
  beforeAll(() => {})

  it('renders with initial content', () => {
    const { getByText } = render(<IframeApp />)
    expect(getByText('Test sentence')).toBeInTheDocument()
  })

  it('sends a postMessage with the new height on mount and resize', () => {
    const postMessageMock = jest.fn()
    parent.postMessage = postMessageMock

    render(<IframeApp />)
    expect(postMessageMock).toHaveBeenCalled()

    act(() => {
      global.dispatchEvent(new Event('resize'))
    })

    expect(postMessageMock).toHaveBeenCalledTimes(2)
  })
})
