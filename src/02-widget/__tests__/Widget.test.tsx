import React from 'react'
import { render } from '@testing-library/react'
import '@testing-library/jest-dom'
import { Widget } from '../Widget'
import { act } from 'react-dom/test-utils'

describe('Widget', () => {
  it('renders an iframe', () => {
    const { getByTestId } = render(<Widget />)
    expect(getByTestId('iframe')).toBeInTheDocument()
  })

  it('updates width on window resize', () => {
    const { getByTestId } = render(<Widget />)
    act(() => {
      global.dispatchEvent(new Event('resize'))
    })
    const iframe = getByTestId('iframeContainer')
    expect(iframe.style.width).not.toBe('0px')
  })
})
