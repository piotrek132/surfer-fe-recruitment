import { useState, useRef, useEffect } from 'react'

import './widget.css'
import { TARGET_ORIGIN } from './const'

export const COMMUNITATION_KEY = 'iframe-height'

export const Widget = () => {
  const [width, setWidth] = useState(0)
  const [height, setHeight] = useState(0)
  const iframeContainer = useRef<HTMLDivElement | null>(null)

  useEffect(() => {
    window.onmessage = function (e) {
      try {
        if (e.origin === TARGET_ORIGIN && e.data?.includes(COMMUNITATION_KEY)) {
          const { height } = JSON.parse(e.data)
          setHeight(height)
        }
      } catch (error) {}
    }
    updateWidth()
    window.addEventListener('resize', updateWidth)
    return () => window.removeEventListener('resize', updateWidth)
  }, [])

  const updateWidth = () => {
    const measurements = iframeContainer.current?.getBoundingClientRect()
    if (measurements) {
      setWidth(measurements?.width)
    }
  }

  return (
    <div className="widget">
      <h1>App content</h1>
      <p>Check out our latest podcast</p>
      <div
        data-testid="iframeContainer"
        className="iframeContainer"
        ref={iframeContainer}
      >
        <iframe
          data-testid="iframe"
          height={height}
          width={width}
          src="/iframe"
          className="iframe"
        />
      </div>
    </div>
  )
}
