import React, { useLayoutEffect, useMemo } from 'react'
import { PillData } from './data'
import { Pill } from './Pill'

interface PillsProps {
  pills: PillData[]
  headers: string[] // ids of pills that are toggled on
  toggleHeader: (id: string) => void
}

enum LayoutElementType {
  LineBreak,
  Pill,
}

interface LayoutBreakElement {
  index: string
  type: LayoutElementType.LineBreak
}

interface LayoutPillElement {
  index: string
  type: LayoutElementType.Pill
  pill: PillData
}

type LayoutElement = LayoutBreakElement | LayoutPillElement

const PILL_WITH_HEADER_WIDTH = 17.63 + 4 // 17.63 is the width of the header icon, 4 is the margin

export function Pills({ pills, headers, toggleHeader }: PillsProps) {
  const containerNode = React.useRef<HTMLDivElement>(null)
  const pillRefs = React.useRef<{ [id: PillData['id']]: HTMLDivElement }>({})
  const [layoutElements, setLayoutElements] = React.useState<LayoutElement[]>(() => {
    return pills.map(pill => ({
      index: pill.id,
      type: LayoutElementType.Pill,
      pill: pill,
    }))
  })
  const memoPills = useMemo(() => pills, [pills])

  useLayoutEffect(() => {
    setLayoutElements(getPills())

    const handleResize = () => {
      setLayoutElements(getPills())
    }
    window.addEventListener('resize', handleResize)
    return () => window.removeEventListener('resize', handleResize)
  }, [memoPills, window, headers])

  const setPillRef = (id: PillData['id'], node: HTMLDivElement) => {
    if (node) {
      pillRefs.current[id] = node
    }
  }

  const getPills = () => {
    const newPills: LayoutElement[] = []
    const maxContainerWidth =
      containerNode.current?.getBoundingClientRect().width || window.innerWidth
    const pillsWithRefs = memoPills.map(pillData => [pillData, pillRefs.current[pillData.id]]) as [
      PillData,
      HTMLDivElement,
    ][]

    let currentWidth = 0
    for (const [pillData, el] of pillsWithRefs) {
      const { id } = pillData
      const isHeader = headers.includes(id)
      const maxPillWidth = Math.ceil(el.offsetWidth + (isHeader ? 0 : PILL_WITH_HEADER_WIDTH))

      currentWidth += maxPillWidth

      if (currentWidth > maxContainerWidth) {
        newPills.push({
          type: LayoutElementType.LineBreak,
          index: `${el.id} ${id} break`,
        })
        currentWidth = maxPillWidth
      }
      newPills.push({
        type: LayoutElementType.Pill,
        index: pillData.id,
        pill: pillData,
      })
    }
    return newPills
  }

  return (
    <div ref={containerNode}>
      {layoutElements.map(el => {
        if (el.type === LayoutElementType.LineBreak) {
          return <br key={`__${el.type}-${el.index}`} />
        } else {
          return (
            <Pill
              key={el.pill.id}
              header={headers.includes(el.pill.id)}
              onClick={() => toggleHeader(el.pill.id)}
              ref={element => element && setPillRef(el.pill.id, element)}
            >
              {el.pill.value}
            </Pill>
          )
        }
      })}
    </div>
  )
}
