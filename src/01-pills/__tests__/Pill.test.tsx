import React from 'react'
import { render, screen, fireEvent } from '@testing-library/react'
import '@testing-library/jest-dom'
import { Pill } from '../Pill'

describe('Pill', () => {
  it('renders the component without errors', () => {
    render(<Pill header={false}>Test</Pill>)
    expect(screen.getByText('Test')).toBeInTheDocument()
  })

  it('displays the "H" label when the `header` prop is set to `true`', () => {
    render(<Pill header={true}>Test</Pill>)
    expect(screen.getByText('H')).toBeInTheDocument()
  })

  it('does not display the "H" label when the `header` prop is set to `false`', () => {
    render(<Pill header={false}>Test</Pill>)
    expect(screen.queryByText('H')).toBeNull()
  })

  it('calls the function passed in the `onClick` prop when clicked', () => {
    const handleClick = jest.fn()
    render(
      <Pill
        header={false}
        onClick={handleClick}
      >
        Click Me
      </Pill>
    )

    fireEvent.click(screen.getByText('Click Me'))
    expect(handleClick).toHaveBeenCalledTimes(1)
  })

  it('should accept a ref as a prop', () => {
    const ref = React.createRef<HTMLDivElement>()
    render(
      <Pill
        header={false}
        ref={ref}
      >
        Ref Test
      </Pill>
    )

    expect(ref.current).toBeInstanceOf(HTMLDivElement)
  })
})
