import React from 'react'
import { render, screen, fireEvent } from '@testing-library/react'
import '@testing-library/jest-dom'
import { Pills } from '../Pills'
import { examplePills } from '../data'

// Mock headers array
const mockHeaders = ['3', '5']

// Mock toggleHeader function
const mockToggleHeader = jest.fn()

describe('Pills', () => {
  it('renders the pills correctly', () => {
    render(
      <Pills
        pills={examplePills}
        headers={mockHeaders}
        toggleHeader={mockToggleHeader}
      />
    )
    // Expect all pills to be in the document
    examplePills.forEach(pill => {
      expect(screen.getByText(pill.value)).toBeInTheDocument()
    })
  })

  it('renders the header indicator for headers', () => {
    render(
      <Pills
        pills={examplePills}
        headers={mockHeaders}
        toggleHeader={mockToggleHeader}
      />
    )
    // Expect the header pills to have the header indicator
    mockHeaders.forEach(headerId => {
      const pill = examplePills.find(p => p.id === headerId) as any
      expect(screen.getByText(pill.value).parentNode).toHaveTextContent('H')
    })
  })

  it('does not render header indicator for non-headers', () => {
    render(
      <Pills
        pills={examplePills}
        headers={mockHeaders}
        toggleHeader={mockToggleHeader}
      />
    )
    // Expect non-header pills not to have the header indicator
    const nonHeaders = examplePills.filter(pill => !mockHeaders.includes(pill.id))
    nonHeaders.forEach(pill => {
      expect(screen.getByText(pill.value).parentNode).not.toHaveTextContent('H')
    })
  })

  it('calls toggleHeader when a pill is clicked', () => {
    render(
      <Pills
        pills={examplePills}
        headers={mockHeaders}
        toggleHeader={mockToggleHeader}
      />
    )
    // Click on each pill
    examplePills.forEach(pill => {
      fireEvent.click(screen.getByText(pill.value))
    })
    // Expect toggleHeader to have been called once for each pill
    expect(mockToggleHeader).toHaveBeenCalledTimes(examplePills.length)
  })
})
